# Adidas

This project executes two sets of tests (unit and integration) against one API created
by [crudcrud](https://crudcrud.com/) website and exposes the test results as an HTML report on GitLab
pages [https://jantoniolopezt.gitlab.io/addidas_api/spark](https://jantoniolopezt.gitlab.io/addidas_api/spark). This
project was developed using **Maven, Java, RestAssured, TestNG and Cucucmber with the integration of extentreports for
reporting purpose.**

## Getting started

Here are the topics in which the project consist:

1. [Test Plan](#test-plan)
2. [Execute test automatically through CI/CD Pipeline](#execute-tests-automatically-through-cicd-pipeline)
3. [Execute test in your local computer](#execute-test-in-your-local-computer)

## Test Plan

**Product:** Adidas API  
**Resposable:** Jose Antonio Lopez Torres  
**Role:** QA Automation  
**System under test:** REST API of pets via [petstore](https://petstore.swagger.io/#/)  
**Environment:** CI/CD Pipeline (Gitlab shared runners) or locally  
**Features to be tested:**

- Search all pets by status
- Creation of a pet
- Search for a pet by id
- Update a pet status
- Delete a pet

**Test Coverage:**

- Integration Tests
- End to End Tests

### Integration Tests

| No. | Scenario |
| ------ | ------ |
| 1 | Validate that all available pets can be searched |
| 2 | Validate that the creation of one available pet is possible |
| 3 | Validate that the modification of a pet status is possible |
| 4 | Validate that the deletion of a pet is possible |

### End to End Tests

| No. | Scenario |
| ------ | ------ |
| 1 | Validate that you can search, create, update and delete a pet |

## Execute tests automatically through CI/CD Pipeline

**FOR THIS OPTION YOU MIGHT NEED PERMISSIONS, PLEASE ASK FOR THEM PROVIDING A GITLAB ACCOUNT**

1. Back again into the [project](https://gitlab.com/jantoniolopezt/addidas_api) you need to **click on the "Pipelines"
   option from the "CI/CD" menu** that is on the left side of the screen.

![Pipelines Option](doc/images/pipelines_option.png)

2. When you land on the Pipelines pages, you need to **trigger a new pipeline** execution by **clicking on the blue "Run
   pipeline" button** that is in the top right corner of the screen.

![Run Pipelines Button](doc/images/run_pipeline.png)

3. Now that you are on the Run pipeline page, **click on the blue "Run pipeline" button**.

![Pipeline Variable](doc/images/variable.png)

4. A new Pipeline execution will be created and you will see **three different jobs (build_job, test_job and pages)**
   that need to **finish successfully** (sometimes a refresh is needed to see the current status of the jobs).

5. Now that the pipeline finished, in order for you to check the **test result report** you will need to go
   to [https://jantoniolopezt.gitlab.io/addias_api/spark](https://jantoniolopezt.gitlab.io/addidas_api/spark) and click
   on the different scenarios so that you are able to see the different steps executed for each test. **The report date
   time is 2 hrs behind the current date time due to the date time of the execution machine.**

![Report](doc/images/report.png)

## Execute test in your local computer

1. First you will need to have installed on your computer the next tools:

- **Git 2.32 or latest**
- **Java JDK 8 or latest**
- **Maven 3.5 or latest**

2.1. Clone the repository

**FOR THIS OPTION YOU MIGHT NEED PERMISSIONS, PLEASE ASK FOR THEM PROVIDING A GITLAB ACCOUNT**

Open GIT Bash terminal from your computer and **locate your self on the location in which you want to download the
project** and write:

- SSH `git clone git@gitlab.com:jantoniolopezt/addias_api.git`
- HTTPS `git clone https://gitlab.com/jantoniolopezt/addias_api.git`

2.2 Download the project as a zip file using the "Download" button from above.

3. Execute tests

- After you cloned or unzip the project a **new folder with the name 'addias_api' will be created**, go inside that folder.
- **On a terminal**, locate your self **inside 'addidas_api' folder** and write the next command **`mvn clean compile`**
  and click enter, wait until it finished with a **'Build success'**.
- After that write **`mvn test`** and click enter, wait until it finished with a **'Build success'**.
- A **new folder with the name 'target' will be created**, go inside **'target > reports'** and you will see a **HTML
  file with the name 'spark.html'**
- Open **'spark.html' on a browser** and you will see the **Test results**.
