Feature: End to End tests for PET service

  Scenario: Validate all possible methods from pets
    When I search for all pets with status "available"
    Then I should receive a 200 HTTP response
    When I create a new pet with status "available"
    Then I should receive a 200 HTTP response
    When I search for the pet
    Then I should receive a 200 HTTP response
    And The status from the pet should be "available"
    When I update the pet status to "sold"
    Then I should receive a 200 HTTP response
    When I search for the pet
    Then I should receive a 200 HTTP response
    And The status from the pet should be "sold"
    When I delete the pet
    Then I should receive a 200 HTTP response
    When I search for the pet
    Then I should receive a 404 HTTP response