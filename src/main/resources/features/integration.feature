Feature: Integration tests for PET service

  Scenario: Retrieve all available pets
    When I search for all pets with status "available"
    Then I should receive a 200 HTTP response

  Scenario: Create a new available pet
    When I create a new pet with status "available"
    Then I should receive a 200 HTTP response
    When I search for the pet
    Then I should receive a 200 HTTP response
    And The status from the pet should be "available"

  Scenario: Sold an available Pet
    Given I have a "available" pet
    When I update the pet status to "sold"
    Then I should receive a 200 HTTP response
    When I search for the pet
    Then I should receive a 200 HTTP response
    And The status from the pet should be "sold"

  Scenario: Delete a Pet
    Given I have a "sold" pet
    When I delete the pet
    Then I should receive a 200 HTTP response
    When I search for the pet
    Then I should receive a 404 HTTP response