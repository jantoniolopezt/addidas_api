package com.adidas.service;

import com.adidas.Endpoints;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class APIService {

    /**
     * Make a GET request to an specific endpoint
     *
     * @param spec     {@link RequestSpecification}
     * @param endpoint URL
     * @return {@link Response}
     */
    public Response get(RequestSpecification spec, String endpoint) {
        return RestAssured.given().spec(spec).get(endpoint);
    }

    /**
     * Make a PUT request to an specific endpoint
     *
     * @param spec     {@link RequestSpecification}
     * @param endpoint URL
     * @return {@link Response}
     */
    public Response put(RequestSpecification spec, String endpoint) {
        return RestAssured.given().spec(spec).put(endpoint);
    }

    /**
     * Make a POST request to an specific endpoint
     *
     * @param spec     {@link RequestSpecification}
     * @param endpoint URL
     * @return {@link Response}
     */
    public Response post(RequestSpecification spec, String endpoint) {
        return RestAssured.given().spec(spec).post(endpoint);
    }

    /**
     * Make a DELETE request to an specific endpoint
     *
     * @param spec     {@link RequestSpecification}
     * @param endpoint URL
     * @return {@link Response}
     */
    public Response delete(RequestSpecification spec, String endpoint) {
        return RestAssured.given().spec(spec).delete(endpoint);
    }

    /**
     * Get a basic {@link RequestSpecification} with out authentication
     *
     * @return {@link RequestSpecification}
     */
    public RequestSpecification getReqSpec() {
        return new RequestSpecBuilder()
                .setBaseUri(Endpoints.BASE_URL)
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .build()
                .filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }

}
