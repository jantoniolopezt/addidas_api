package com.adidas.models;

import com.google.gson.Gson;
import org.testng.Assert;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigInteger;
import java.util.List;

public class Pet {

    private List<String> photoUrls;
    private String name;
    private BigInteger id;
    private Category category;
    private List<Tag> tags;
    private String status;

    public void setPhotoUrls(List<String> photoUrls) {
        this.photoUrls = photoUrls;
    }

    public List<String> getPhotoUrls() {
        return photoUrls;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getId() {
        return id;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "Pet{" +
                        "photoUrls = '" + photoUrls + '\'' +
                        ",name = '" + name + '\'' +
                        ",id = '" + id + '\'' +
                        ",category = '" + category + '\'' +
                        ",tags = '" + tags + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }

    public static Pet createPet() {
        Pet pet = null;
        try {
            pet = new Gson().fromJson(
                    new FileReader("src/main/java/com/adidas/models/raw/pet.json"), Pet.class);
        } catch (FileNotFoundException e) {
            Assert.fail("Unable to create a Pet");
        }
        return pet;
    }
}