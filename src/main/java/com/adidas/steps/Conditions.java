package com.adidas.steps;

import com.adidas.Endpoints;
import com.adidas.models.Pet;
import com.adidas.service.APIService;
import io.cucumber.java.en.Given;
import io.restassured.specification.RequestSpecification;

public class Conditions extends APIService {

    private StepData stepData;

    public Conditions(StepData stepData) {
        this.stepData = stepData;
    }

    @Given("I have a {string} pet")
    public void createPet(String status) {
        Pet pet = Pet.createPet();
        pet.setStatus(status);
        RequestSpecification spec = getReqSpec().body(pet);
        stepData.setResponse(post(spec, Endpoints.PET));
        stepData.setPetId(stepData.getResponse().then().extract().as(Pet.class).getId());
    }

}
