package com.adidas.steps;

import com.adidas.models.Pet;
import io.cucumber.java.en.Then;
import org.junit.Assert;

public class Assertions {

    private StepData stepData;

    public Assertions(StepData stepData) {
        this.stepData = stepData;
    }

    @Then("I should receive a {int} HTTP response")
    public void httpResponse(int httpCode) {
        stepData.getResponse().then()
                .assertThat().statusCode(httpCode);
    }

    @Then("The status from the pet should be {string}")
    public void validatePetStatus(String status) {
        Pet pet = stepData.getResponse().then().extract().as(Pet.class);
        Assert.assertEquals(String.format("Pet status is not equal to %s", status), status, pet.getStatus());
    }
}
