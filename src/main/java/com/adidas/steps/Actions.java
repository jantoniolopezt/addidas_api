package com.adidas.steps;

import com.adidas.Endpoints;
import com.adidas.models.Pet;
import com.adidas.service.APIService;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class Actions extends APIService {

    private StepData stepData;

    public Actions(StepData stepData) {
        this.stepData = stepData;
    }

    @When("I search for all pets with status {string}")
    public void searchPetsByStatus(String status) {
        RequestSpecification spec = getReqSpec();
        spec.queryParam("status", status);
        stepData.setResponse(get(spec, Endpoints.PET_BYSTATUS));
    }

    @When("I create a new pet with status {string}")
    public void createPetWithStatus(String status) {
        Pet pet = Pet.createPet();
        pet.setStatus(status);
        RequestSpecification spec = getReqSpec().body(pet);
        stepData.setResponse(post(spec, Endpoints.PET));
        stepData.setPetId(stepData.getResponse().then().extract().as(Pet.class).getId());
    }

    @When("I search for the pet")
    public void getPetById() {
        clearResponse();
        stepData.setResponse(get(getReqSpec(), String.format(Endpoints.PET_BYID, stepData.getPetId())));
    }

    @When("I update the pet status to {string}")
    public void updatePetStatus(String status) {
        RequestSpecification spec = getReqSpec()
                .contentType(ContentType.URLENC)
                .formParam("status", status);
        stepData.setResponse(post(spec, String.format(Endpoints.PET_BYID, stepData.getPetId())));
    }

    @When("I delete the pet")
    public void deletePet() {
        stepData.setResponse(delete(getReqSpec(), String.format(Endpoints.PET_BYID, stepData.getPetId())));
    }

    private void clearResponse() {
        stepData.setResponse(null);
    }
}
