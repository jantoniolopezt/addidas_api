package com.adidas.steps;

import io.restassured.response.Response;

import java.math.BigInteger;

public class StepData {

    private Response response;
    private BigInteger petId;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public BigInteger getPetId() {
        return petId;
    }

    public void setPetId(BigInteger petId) {
        this.petId = petId;
    }

}
