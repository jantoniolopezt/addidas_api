package com.adidas;

public class Endpoints {

    public static final String BASE_URL = "https://petstore.swagger.io/v2";
    public static final String PET = "/pet";
    public static final String PET_BYSTATUS = "/pet/findByStatus";
    public static final String PET_BYID = "/pet/%s";

}
